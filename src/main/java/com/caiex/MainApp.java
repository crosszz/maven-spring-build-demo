package com.caiex;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class MainApp {
   public static void main(String[] args) {
      ApplicationContext context = 
             new ClassPathXmlApplicationContext("Beans.xml");

      HelloWorld obj = (HelloWorld) context.getBean("helloWorld");

      obj.getMessage();
   }
}

// 1. assembly 
// $ mvn clean compile assembly:single
// windows > java -cp .;target\Cross-Test-0.0.1-SNAPSHOT-jar-with-dependencies.jar com.caiex.MainApp
// linux $ java -cp .:target/Cross-Test-0.0.1-SNAPSHOT-jar-with-dependencies.jar com.caiex.MainApp


// 2. shade
// $ mvn clean package
// $ java -jar target/Cross-Test-0.0.1-SNAPSHOT.jar
