# Maven to build

There are 2 methods listed in the pom.xml.

## Assembly 

- build it:

```
mvn clean compile assembly:single
```

compile/package are same form assembly.


- run it:

on windows
```
java -cp .;target\Cross-Test-0.0.1-SNAPSHOT-jar-with-dependencies.jar com.caiex.MainApp
```

on Linux
```
java -cp .:target/Cross-Test-0.0.1-SNAPSHOT-jar-with-dependencies.jar com.caiex.MainApp
```

As we assembled all dependencies into one jar, so we can run, no matter windows/linux, as following:

```
java -cp target/Cross-Test-0.0.1-SNAPSHOT-jar-with-dependencies.jar com.caiex.MainApp
```


## Shade

- build it:

```
mvn clean package
```

- run it:

```
java -jar target/Cross-Test-0.0.1-SNAPSHOT.jar
```
